import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the CalculateDistancePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'calculateDistance',
})
export class CalculateDistancePipe implements PipeTransform {
  

  /**
   * Takes a value and makes it lowercase.
   */
  transform(destination, origin, convertTo) {
    if (!Array.isArray(origin)){
      origin = origin.split(",");
    }
    
    return this.convert(this.getDistance(destination,origin), 'm','mi');
  }

  getDistance(origin, destination) {
    // return distance in meters
    var lon1 = this.toRadian(origin[1]),
        lat1 = this.toRadian(origin[0]),
        lon2 = this.toRadian(destination[1]),
        lat2 = this.toRadian(destination[0]);

    var deltaLat = lat2 - lat1;
    var deltaLon = lon2 - lon1;

    var a = Math.pow(Math.sin(deltaLat/2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(deltaLon/2), 2);
    var c = 2 * Math.asin(Math.sqrt(a));
    var EARTH_RADIUS = 6371;
    return c * EARTH_RADIUS * 1000;
  }
  
  toRadian(degree) {
      return degree*Math.PI/180;
  }

  convert(distance, from, to) {
    let meters_in_mile = 1609;
    let meters_in_km = 1000;
    let threshold = meters_in_mile;

    if ( to == 'km') {
      threshold = meters_in_km;
    }

    if (distance <= threshold/2) {
       return distance.toFixed(0) + 'm';
     }

     let conversionKey = {
          m: {
              km: 0.001,
              m:1,
              mi:0.000621371
          },
          km: {
              km:1,
              m: 1000,
              mi:0.621371
          },
          mi: {
              km:1.60934,
              m: 1609.34,
              mi:1
          }
      };
      
      return (distance * conversionKey[from][to]).toFixed(2) + to;
   };
}
