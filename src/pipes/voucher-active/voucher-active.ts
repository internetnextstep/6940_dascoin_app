import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the VoucherActivePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'voucherActive',
})
export class VoucherActivePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(vouchers) {
    let activeVouchers = [];
    vouchers.forEach(element => {
      if (element.statusTy == 0) {
        activeVouchers.push(element);
      }
    });
    return activeVouchers;
  }
}
