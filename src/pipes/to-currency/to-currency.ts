import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyProvider} from '../../providers/currency/currency';
/**
 * Generated class for the ToCurrencyPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'toCurrency',
})
export class ToCurrencyPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  constructor(public currencyProvider: CurrencyProvider) {

  }

  transform(value: string, currencyTy) {
    let transformedCurrency = '';

    return this.currencyProvider.getLocalCurrencies().then((currencies:any[]) => {
      currencies.forEach(element => {
        if (element.CurrencyTy == currencyTy) {
          
          transformedCurrency = element.Symbol + parseFloat(value).toFixed(element.Cents);
        }
      });
      
      return transformedCurrency;
    });
  }
}
