import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the VoucherRedeemedPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'voucherRedeemed',
})
export class VoucherRedeemedPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(vouchers: any) {
    let redeemedVouchers = [];
    vouchers.forEach(element => {
      if (element.statusTy == 2) {
        redeemedVouchers.push(element);
      }
    });
    return redeemedVouchers;
  }
}
