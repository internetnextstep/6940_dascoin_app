import { NgModule } from '@angular/core';
import { VoucherActivePipe } from './voucher-active/voucher-active';
import { VoucherRedeemedPipe } from './voucher-redeemed/voucher-redeemed';
import { ToCurrencyPipe } from './to-currency/to-currency';
import { VoucherRedeemDatePipe } from './voucher-redeem-date/voucher-redeem-date';
import { CalculateDistancePipe } from './calculate-distance/calculate-distance';
import { TimestampToRelativePipe } from './timestamp-to-relative/timestamp-to-relative';
@NgModule({
	declarations: [VoucherActivePipe,
    VoucherRedeemedPipe,
    ToCurrencyPipe,
    VoucherRedeemDatePipe,
    CalculateDistancePipe,
    TimestampToRelativePipe],
	imports: [],
	exports: [VoucherActivePipe,
    VoucherRedeemedPipe,
    ToCurrencyPipe,
    VoucherRedeemDatePipe,
    CalculateDistancePipe,
    TimestampToRelativePipe]
})
export class PipesModule {}
