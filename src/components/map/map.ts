import { Component, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import * as L from 'leaflet';
import { Geolocation } from '@ionic-native/geolocation';
/**
 * Generated class for the MapComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'map',
  templateUrl: 'map.html'
})
export class MapComponent {
  @Input('vendorLocations') vendorLocations: any[] = [];
  @Input('draggable') draggable: any = true;
  @Input('zoomLevel') zoomLevel: any = 15;
  @Input('zoomControl') zoomControl: any = true;
  @Input('userLocation') userLocation: any = null;
  @Input('mapContainerId') mapContainerId:string = 'map';
  @Input('hasPopups') hasPopups:boolean = true;

  map: any;
  accessToken:any = 'pk.eyJ1IjoiaW50ZXJuZXRuZXh0c3RlcCIsImEiOiJjaml6M3dwam4wMzJ3M2twY3ZneXN3d3g3In0.pae_cAKXlDmPOmV-WmFVtQ';
  
  userInitial:any;
  text: string;

  marker_user = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAnCAYAAACMo1E1AAAFV0lEQVRYhe2YW2xURRjHf3PO9mZ3F7ptSLdL2yUkQsslkVaMphgRQ0LkQY0K0YSgxltSogkmBvES4+2F+NJq4gMqKlHogz7wAGLwAYIJdEUhtqAxS0l3l0vZQndLb7tnfDgHevacOdsbvvFPTtLOzPf//vvNfPPNDNzB7CBu/bXtwExta4GVwBIgDNQA5VbfKDAApIBzwGng4ozYOzbim6GgEqANeARYVGSc3/qiwP1WWxz4BTgGTEzH2UzEPQBsxozQbLAIeBF4HPgBOD6VwXTE+YGXgZZZinKiBmjHjOgXQNZroDYFURj48DYKs6PF4g57DSgWuTrgXSDoOUKIMoQ2HyECIMptfDmQo0iZQRrXkHLMg2EB8B7wAZBwdnpFbh7wlrcwUYrmi6L5mhFaHYgAZrII6ysBEUBodWi+ZjRfFCFKPXwFgR2WzynFCWAbEFLr0kLovqUIUeXhTGEjqtB8SxGamtP0tQ371uYhbh3QrKTQ9AVoeiOgT1vYJHQ0vRFNX+DR34y5RXmKuwvYpDQVWhVCi8xClJMnUiSCT1saAHdCrAcqFYwlaHqDii0yv0zf/lA0tKGpxh+tqijxaULEB0fGD/YOZD86HE9fyozlXUaaXk9eZkGOO3oqLQ0/QWHkBPCw8vdoehjFEti6ui7w9462xa892FB9fTRftqc7pe0+kRSDN3Jl7W0N1f++3bZ4S2tdQMWIptcqfZkaBBRGbgmq3V+IUoRwTcNz90UCuzcti/yRzPDCvl5OJTIF/fdEAny1uVn7+pnlEQmJb7uThQOECCHERaQrejWWlrP2aKxQ/g6hVeHIosZQha/ziaXhWP8QazpjLmEApxIZ2jq7OZXI8PlTTeFwsMyZRMLiVmEFFE6VupAL4ZqWN9Y2hspLNO35fb0Mj7uX1E1kx/K81NVLZYmubV8bdQtRcNu12MV5ZKIod7asX1JTeeLCEGdSnmXxFmL9GWL9QzzaXKMQ4ua2a7GL83sMdJW4hvnlpbF+91R64XQqy6JQhapCeJVPv1NcxXSdlehi6kFzQwUUihvxGJhzNsTTI+Ot9d7nASdWhv3E0yOq4u/itmuxi/NYQHLU2XLw7EB2dX2QlWGvlTCJ1vogLQuDHPjrioLfzW3XYhfnOrKY9tK1uHYd6Rscmcgb3z27DL9rh5jEvHIfX25qYngib+z6tW9wOtx2LXZxcbU4YxCQ9qa+wZHcq129qeW1fo61t7JqoTsRW+uDHG1vYXmtn1f296QUZUxa3CrEoTBbzmCe7x0Uchwp0whRbW/+pjuZEYLEZ082hbtfX63F+of43dqMWxYGWRUJkBnLG1v2nkntjaXcEZIyragOdi0F4s5hXufcJczIp9B9VTjq656TyczBs1dvvLkuWrWhqSaw9d5wKUD86uh4x7EL2Y+9Cj8YGHmvq+KApcV1b30M89jihtBC1llu7jDyfUgj7dHbBfxIx0bXSeNnYFhpIo000lAnzUwgjUQRYcPAoZv/OMXdAPZ7Ehv5yxj5PsCYhSwDI9+Hkb9cZEyXpUEpDsxbeY+nuTTSGLlepPTKNIWNHDRtPCOG5fOwvUElTgIdgDeRlOMYufMYuR6kkQSZofCJYQJkBmkkMXI9GLnzRTITy1cnji3Lq/BeBz4B3qHYvVXKMWT+EnCpiOOpMGT5uubsKHbjTwDvA1fm4HgqXLF8KBNtqueIFLATiN1mUVicOy0fSkznIScLfIr58DKXV6abuAp8D/yGY43NRhwWyXHgJLAG8/IbnaGo85g7wVH+h/c5LNIj1hfGfNm8G/PRp5rJA+sIZoSSwD/AnxSZvju43fgPysazJ+AFYuIAAAAASUVORK5CYII=';

  constructor(
    public elementRef: ElementRef,
    public navCtrl: NavController,
    public geolocation :Geolocation) {
    
  }

  ngAfterViewInit(){
    let $this = this;
    $this.init();
  }

  init() {
    let $this = this;
    let mapCenter = $this.userLocation !== null && $this.userLocation !== undefined ? $this.userLocation : this.vendorLocations[0].latLng;
   
    this.map = L.map(this.mapContainerId, { zoomControl: $this.zoomControl})
      .setView(mapCenter, $this.zoomLevel);

   L.tileLayer('https://api.tiles.mapbox.com/v4/mapbox.streets-basic/{z}/{x}/{y}.png?access_token=' + this.accessToken, {
      attribution: 'Powered by NetLeaders',
      accessToken: this.accessToken,
      zoom: $this.zoomLevel
    }).addTo(this.map);
    
    if (this.vendorLocations.length > 0) {
      let featureGroupMarkers:any = this.addMarkers(this.vendorLocations);
    }    

    $this.map.invalidateSize();​

    if (this.draggable === false ) {
      $this.disableMap();
    }
  } 

  showVendor(location) {
    this.navCtrl.push('VendorPage', {vendor: location });
  }

  addMarkers(markers) {
    let $this = this;
    let featureGroupMarkers = [];

    this.vendorLocations.forEach((location) => {
        let marker = L.marker(L.latLng(location.latLng[0], location.latLng[1]));
        
        if ($this.hasPopups === true) {
          marker.bindPopup(L.popup({maxWidth:300, minWidth:300, maxHeight: 200}).setContent("<div class='popup-custom' style='background-image:url("+location.vendorLogoUrl+");'><p class='showVendor' data-vendorId='"+location.vendorId+"'>" + location.vendorDescription + "</p><h4  class='showVendor' data-vendorId='"+location.vendorId+"' >"+ location.vendorName +"</h4></div>"));

          marker.on('click', function(){
            marker.openPopup();
          });

          marker.on('popupopen', function() {
            setTimeout(function() {
              $this.elementRef.nativeElement.querySelectorAll('.showVendor').forEach( function ( item ) {

                item.addEventListener('click', (e)=>
                {
                  // get id from attribute
                  var vendorId = e.target.getAttribute("data-vendorId");
                  $this.showVendor(location);
                });
              });
            },500);          
          }); 
        }

        featureGroupMarkers.push(marker);
    });
    
    let featureGroup = L.featureGroup(featureGroupMarkers).addTo($this.map);
      
    if (featureGroupMarkers.length > 1) {
      $this.map.fitBounds(featureGroup.getBounds().pad(0.5));
    }

    return featureGroupMarkers;
  }

  addUserLocation() {
    return new Promise(function(resolve,reject) {
      this.geolocation.getCurrentPosition().then((resp) => {
        let latLng = [resp.coords.latitude, resp.coords.longitude];
        var userIcon = L.icon({
            iconUrl: this.marker_user,
        });

        let marker = L.marker(L.latLng(latLng[0], latLng[1]), { icon: userIcon});
        marker.addTo(this.map);
        this.map.flyTo(L.latLng(latLng[0], latLng[1]));
        resolve();
      }).catch((error) => {
        console.log('Error getting location', error);
        reject();
      });
    });
    
  }

  disableMap() {
    this.map.dragging.disable();
    this.map.touchZoom.disable();
    this.map.doubleClickZoom.disable();
    this.map.scrollWheelZoom.disable();
  }



}
