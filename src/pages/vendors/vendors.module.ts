import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VendorsPage } from './vendors';
import { VendorProvider } from '../../providers/vendor/vendor';
import { Geolocation } from '@ionic-native/geolocation';
import { IonicStorageModule } from '@ionic/storage';
import { ComponentsModule } from '../../components/components.module';
import { Ionic2RatingModule } from "ionic2-rating";
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    VendorsPage,
  ],
  imports: [
    IonicPageModule.forChild(VendorsPage),
     IonicStorageModule.forRoot(),
     ComponentsModule,
     Ionic2RatingModule,
     PipesModule
  ],
  providers: [VendorProvider,Geolocation]
})
export class VendorsPageModule {}
