import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { VendorProvider } from '../../providers/vendor/vendor';
import { Geolocation } from '@ionic-native/geolocation';
import { Storage } from '@ionic/storage';
import { Ionic2RatingModule } from "ionic2-rating";
import * as _ from 'underscore';

/**
 * Generated class for the VendorsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vendors',
  templateUrl: 'vendors.html',
})
export class VendorsPage {
  vendor_list:any;
  isLoading:any = true;
  loaded_list:any = [];
  location:any = false;
  showMaps:any = false;
  markers:any = [];
  showSearchBox: any = false;
  isBookmark: any = false;
  _title: any = 'Vendors';
  title: any = '';
  radius = 100;
  searchQuery = '';
  vendorTypes = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public vendorProvider: VendorProvider,
    public modalCtrl: ModalController,
    public geolocation: Geolocation,
    public storage: Storage) {
      this.title = this._title;
      this.isBookmark = this.navParams.data === true ? true : false;
  }

  ionViewDidLoad() {
    this.initList();
  }

  initList() {
    let $this = this;

    if (this.isBookmark === true) {
      $this.getLocation().then((coords:any) => {
       this.showBookmarks();
      }).catch((e) => {
       this.showBookmarks();
      });
    } else {
      $this.vendorProvider.getVendorTypes().then((r:any) => {
        $this.vendorTypes = r;
      });
      
      $this.getLocation().then((coords:any) => {
        $this.getVendorList(null, { userLatLng: coords });
      }).catch((e) => {
        $this.getVendorList(null, null);
      });
    }    
  }

  loadDefaultImage(event) {
    event.target.src = "assets/imgs/blank.png";
  }

  openVendorPage(vendor) {
    this.showSearchBox = false;
    this.showMaps = false;
    this.navCtrl.push('VendorPage', {vendor: vendor, location: this.location });
  //  modal.present();
  }

  getVendorList(refresher, opts) {
    this.isLoading = true;
    let userLatLng:any = opts !== null && opts.userLatLng !== undefined ? opts.userLatLng : this.location;
    let searchQuery: any = opts !== null && opts.searchQuery !== undefined ? opts.searchQuery : this.searchQuery;
    let radius: any = opts !== null && opts.radius !== undefined ? opts.radius : this.radius;

    if (this.isBookmark === true) {
      this.showBookmarks();

      if (refresher !=null) {
        refresher.complete();
      }

      return false;
    }

    this.vendorProvider.getList({ 
      userLatLng: userLatLng, 
      radius: radius, 
      searchQuery: searchQuery
    }).then((list:any) => {
      this.vendor_list = list;
      this.loaded_list = list;

      this.isLoading = false;

      if (refresher !=null) {
        refresher.complete();
      }

    }).catch(() => {
      
      if (refresher !=null) {
        refresher.complete();
      }
    });


  }

  getItems(ev: any) {
    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.loaded_list = this.vendor_list.filter((item:any) => {
        return (item.vendorName.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    } else {
      this.loaded_list = this.vendor_list;
    }
  }
  
  showPermissionPage() {
    let permissionModal = this.modalCtrl.create( 'LocationPermissionPage', { });
    let $this = this;
    permissionModal.present();

    permissionModal.onDidDismiss(() => {
      $this.initList();
    });
  }

  getLocation() {    
    let $this = this;
    
    return new Promise(function(resolve,reject) {
        
      $this.storage.get('SHOWN_PERMISSION_LOCATION_PAGE').then((r) => {
        if (r === null) {
          $this.showPermissionPage();
        } else {
          $this.geolocation.getCurrentPosition().then((resp) => {
            $this.location = resp.coords.latitude +',' + resp.coords.longitude;

            resolve($this.location);
          }).catch((error) => {
            reject();
          });
        }
      }).catch(() => {
          $this.showPermissionPage();
      });
    });
  }

  getVendorLocations() {
    let loaded_locations = [];
    
    this.loaded_list.forEach((item:any) => {
      loaded_locations.push(item);
    });

    this.markers = loaded_locations;

    return loaded_locations;
  }

  showMap() {
    this.getVendorLocations();
    this.showMaps = true;   
    this.showSearchBox = false; 
  }

  showList() {
     this.showMaps = false;
     this.showSearchBox = false;
  }

  showSearch() {
    this.showSearchBox = true;
    this.showMaps = false;
  }

  showBookmarks() {
    if (this.isBookmark === true ) {
      this.title = 'Bookmarks';
      this.vendorProvider.getBookmarks().then((bookmarks) =>  {
        this.loaded_list = _.uniq(bookmarks, 'id');
        this.isLoading = false;
      });
    } else {
      this.title = this._title;
      this.loaded_list = this.vendor_list;
    }
  }

  filterChange(e) {
    let $this = this;
    let searchQuery = this.searchQuery == 'category' ? '' : this.searchQuery;
    let radius = this.radius;

    $this.getLocation().then((coords:any) => {
      $this.getVendorList(null, { userLatLng: coords, radius: radius, searchQuery: searchQuery  });
    }).catch((e) => {
      $this.getVendorList(null, null);
    });
  }


}
