import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VoucherImagePage } from './voucher-image';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    VoucherImagePage,
  ],
  imports: [
    IonicPageModule.forChild(VoucherImagePage),
    PipesModule
  ],
})
export class VoucherImagePageModule {}
