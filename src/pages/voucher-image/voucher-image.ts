import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController} from 'ionic-angular';
import { QRCode, ErrorCorrectLevel, QRNumber, QRAlphaNum, QR8BitByte, QRKanji } from 'qrcode-generator-ts/js';
import { VoucherProvider } from '../../providers/voucher/voucher';

/**
 * Generated class for the VoucherImagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-voucher-image',
  templateUrl: 'voucher-image.html',
  providers: [QRCode]
})
export class VoucherImagePage {
  qrImageUri:any;
  voucher: any;
  checkVoucherRedemptionTimer:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public qrcode: QRCode,
    public viewCtrl: ViewController,
    public voucherProvider: VoucherProvider,
    public toastCtrl: ToastController) {
      this.voucher = this.navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VoucherImagePage');
    this.generateQR({ voucherId: this.voucher.voucherId, amount:this.voucher.amount, currencyTy: this.voucher.currencyTy, voucherCode: this.voucher.voucherCode})
  }

  ionViewWillLeave() {
    clearTimeout(this.checkVoucherRedemptionTimer);
    this.checkVoucherRedemptionTimer = null;
  }

  generateQR(voucher) {
    let typeNumber = 6;
    let errorCorrectionLevel = 'L';
    this.qrcode.setErrorCorrectLevel(ErrorCorrectLevel.L);
    this.qrcode.setTypeNumber(typeNumber);
    this.qrcode.clearData();
    this.qrcode.addData(JSON.stringify(voucher));
    this.qrcode.make();
    this.qrImageUri = this.qrcode.toDataURL(5);

    let toast = this.toastCtrl.create({
      message: 'Voucher Created, allow the vendor to scan this QR code',
      duration: 4000,
      position: 'bottom'
    });

    toast.present();
    
    this.checkVoucherRedemption(voucher.voucherId, voucher.voucherCode);
  };

  dismiss() {
    this.viewCtrl.dismiss();
  }

   checkVoucherRedemption(voucherId, voucherCode){
     let $this = this;
     $this.voucherProvider.isVoucherRedeemed(voucherId, voucherCode).then((isRedeemed) => {
       if (isRedeemed == true) {
          let toast = $this.toastCtrl.create({
            message: 'Voucher redeemed successfully',
            duration: 4000,
            position: 'bottom'
          });

          toast.present();

          $this.dismiss();
       } else {
         if ($this.checkVoucherRedemptionTimer === null) { return false; }
         $this.checkVoucherRedemptionTimer = setTimeout(()=> {
          $this.checkVoucherRedemption(voucherId, voucherCode);
         }, 2500);
       }
     });
   }

}
