import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, App } from 'ionic-angular';
import { MemberProvider } from '../../providers/member/member';
import { AppCommon } from '../../providers/app';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  firstName: any;
  lastName: any;
  email:any; 
  phone: any;
  username: any;
  mi_km:any = 'mi';
  appVersion:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public memberProvider: MemberProvider,
    public toastCtrl: ToastController,
    public storage: Storage,
    public app: App,
    public appCommon: AppCommon) {
      this.appVersion = this.appCommon.APP_VERSION;

      this.memberProvider.getUser().then((user:any) =>  {
        this.firstName = user.firstName;
        this.lastName = user.lastName;
        this.email = user.email;
        this.phone = user.phone;
        this.username = user.username;
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  saveAccount() {
    this.memberProvider.updateCustomer(this.firstName, this.lastName, this.email, this.phone).then(()=>{
      this.toastCtrl.create({ duration: 3000, message: 'Details updated successfully' });
    });
  }

  logout() {
    this.storage.remove('user');
    this.storage.remove('bookmarks');

    this.app.getRootNav().setRoot('LoginPage');

  }

  launchTerms() {
    window.open(this.appCommon.SYSTEM_URL + '/base/terms-of-use');
  }

  launchPrivacyPolicy() {
    window.open(this.appCommon.SYSTEM_URL + '/base/privacy-policy');
  }

}
