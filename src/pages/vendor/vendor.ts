import { Component,ChangeDetectorRef,NgZone } from '@angular/core';
import { IonicPage, ModalController,ToastController, NavController, NavParams, ViewController } from 'ionic-angular';
import { Ionic2RatingModule } from "ionic2-rating";
import { Geolocation } from '@ionic-native/geolocation';
import { VendorProvider } from '../../providers/vendor/vendor';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the VendorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vendor',
  templateUrl: 'vendor.html',
})
export class VendorPage {
  vendor: any;
  markers :any = [];
  reviewCountText: any = 0;
  location: any;
  rating:any = 0;
  public ratingValues = [0, 0, 0, 0, 0];
  vendorBookmarked = "gray"
  bookmarkText = 'BOOKMARK';
  menuClassName = 'seethrough';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public geolocation: Geolocation,
    public vendorProvider: VendorProvider,
    public toastCtrl: ToastController,
    public socialSharing: SocialSharing,
    private changeDetectorRef:ChangeDetectorRef,
    public zone: NgZone) {
    this.vendor = navParams.get('vendor');
    this.markers.push(this.vendor);
    this.location = this.navParams.get('location');
    let ending = this.vendor.reviews.length == 1 ? 'review' : 'reviews';
    this.reviewCountText = this.vendor.reviews.length + ' ' + ending;
    for (let key in this.vendor.reviews) {
      this.ratingValues[this.vendor.reviews[key].rating - 1]++;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VendorPage');
    let $this = this;

    this.toggleVendorBookmarked();
    
  }

  loadDefaultImage(event) {
    event.target.src = "assets/imgs/blank.png";
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

  showInteractiveMap() {
     let modal = this.modalCtrl.create('MapPage', { markers: this.markers});

     modal.present();
  }

  call() {
    window.location.href = "tel: " + this.vendor.phone;
  }

  showReviews() {
    let modal = this.modalCtrl.create('VendorReviewsPage', { vendor: this.vendor });
    modal.present();
  }

  showGallery() {
    let modal = this.modalCtrl.create('GalleryPage', { vendor: this.vendor });
    modal.present();
  }

  scrolling(event) {

    this.zone.run(()=>{
      if (event.scrollTop > 99 && event.directionY == 'down' && this.menuClassName != 'solid') {
        this.menuClassName = 'solid';
      }

      if (event.scrollTop < 100 && event.directionY == 'up' && this.menuClassName != 'seethrough') {
        this.menuClassName = 'seethrough';
      }
    })
    
  }

  // rate function
  rate(stars) {
    this.rating = stars;

    let modal = this.modalCtrl.create('RateVendorPage', { rating: this.rating, vendor: this.vendor });
    modal.present();
  }

  // make array with range is n
  range(n) {
    return new Array(Math.round(n));
  }

  bookmarkVendor() {
    let $this = this;

    this.vendorProvider.isVendorBookmarked(this.vendor.vendorId).then((isVendorBookmarked) => {
      if (isVendorBookmarked === false) {
        this.vendorProvider.putBookmark(this.vendor.vendorId).then(() => {
          let toast = $this.toastCtrl.create( { message: 'Saved to bookmarks', duration: 2500});
          toast.present();
          $this.toggleVendorBookmarked();
        });
      } else {
        this.vendorProvider.deleteBookmark(this.vendor.vendorId).then(() => {
          let toast = $this.toastCtrl.create( { message: 'Removed from bookmarks', duration: 2500})
          toast.present();
          $this.toggleVendorBookmarked();
        });
      }
    });

  }

  share() {
    this.socialSharing.share('','','','https://smartbusinesstools.com/');
  }

  toggleVendorBookmarked() {
    let $this = this;
    this.vendorProvider.isVendorBookmarked(this.vendor.vendorId).then((isVendorBookmarked) => {
      if (isVendorBookmarked === true) {
        $this.vendorBookmarked = 'primary';
        $this.bookmarkText = 'BOOKMARKED'
      } else {
        $this.vendorBookmarked = 'gray';
        $this.bookmarkText = 'BOOKMARK'
      }

      $this.changeDetectorRef.detectChanges();
    });
  }
}
