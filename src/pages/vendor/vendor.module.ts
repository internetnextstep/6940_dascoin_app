import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VendorPage } from './vendor';
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';
import { Ionic2RatingModule } from "ionic2-rating";
import { Geolocation } from '@ionic-native/geolocation';
import { VendorProvider } from '../../providers/vendor/vendor';
import { SocialSharing } from '@ionic-native/social-sharing';

@NgModule({
  declarations: [
    VendorPage,
  ],
  imports: [
    IonicPageModule.forChild(VendorPage),
    PipesModule,
    ComponentsModule,
    Ionic2RatingModule
  ],
  providers: [Geolocation,VendorProvider, SocialSharing]
})
export class VendorPageModule {}
