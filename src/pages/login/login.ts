import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { AppCommon } from '../../providers/app';
import { MemberProvider } from '../../providers/member/member';
import { SplashScreen } from '@ionic-native/splash-screen';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  login_segment: any = 'login';
  username: any = '';
  password: any = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public appCommon: AppCommon,
    public member: MemberProvider,
    public toastCtrl : ToastController,
    public splashScreen: SplashScreen,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.splashScreen.hide();
  }

  btnLogin() {
     let loader = this.loadingCtrl.create({
      content: "Logging In...",
      duration: 30000
    });

    loader.present();

    this.member.login(this.username, this.password).then((r) => {
      loader.dismiss();

      this.navCtrl.setRoot('HomePage');
    }).catch(() => {
      let toast = this.toastCtrl.create({
        message: 'Login Failed. Please try again',
        duration: 3000
      });

      loader.dismiss();
      
      toast.present();

    });
  }

}
