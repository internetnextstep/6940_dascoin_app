import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransactionsPage } from './transactions';
import { MemberProvider } from '../../providers/member/member';

@NgModule({
  declarations: [
    TransactionsPage,
  ],
  imports: [
    IonicPageModule.forChild(TransactionsPage),
  ],
  providers: [MemberProvider]
})
export class TransactionsPageModule {}
