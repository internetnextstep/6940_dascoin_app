import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { MemberProvider } from '../../providers/member/member';

/**
 * Generated class for the TransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-transactions',
  templateUrl: 'transactions.html',
})
export class TransactionsPage {
  transactions:any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public memberProvider: MemberProvider, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransactionsPage');
    this.loadTransactions();
  }

  loadTransactions() {
    this.memberProvider.getTransactions(200).then((transactions:any) => {
      this.transactions = transactions;
    })
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
