import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocationPermissionPage } from './location-permission';
import { Geolocation } from '@ionic-native/geolocation';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  declarations: [
    LocationPermissionPage,
  ],
  imports: [
    IonicPageModule.forChild(LocationPermissionPage),
     IonicStorageModule.forRoot()
  ],
  providers: [ Geolocation]
})
export class LocationPermissionPageModule {}
