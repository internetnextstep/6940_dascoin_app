import { Component } from '@angular/core';
import { IonicPage, ViewController, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LocationPermissionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-location-permission',
  templateUrl: 'location-permission.html',
})
export class LocationPermissionPage {

  constructor(
      public navCtrl: NavController, 
      public navParams: NavParams,
      private geolocation: Geolocation,
      public viewCtrl: ViewController,
      public storage:Storage) {
  }

  ionViewDidLoad() {
    let $this = this;
    
    $this.storage.set('SHOWN_PERMISSION_LOCATION_PAGE', true).then(() => {});

    this.geolocation.getCurrentPosition().then((resp) => {
      $this.viewCtrl.dismiss();
    }).catch((error) => {
      $this.viewCtrl.dismiss();
    });
  }

}
