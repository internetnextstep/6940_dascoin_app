import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, ViewController } from 'ionic-angular';
import { MemberProvider } from '../../providers/member/member';
import { ToCurrencyPipe } from '../../pipes/to-currency/to-currency';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

/**
 * Generated class for the TransferPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-transfer',
  templateUrl: 'transfer.html',
})
export class TransferPage {
  address: any = '';
  amount: any = '';
  balance: any = 0;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public memberProvider: MemberProvider,
    public toastCtrl: ToastController,
    public viewCtrl: ViewController,
    public toCurrencyPipe: ToCurrencyPipe,
    public barcodeScanner: BarcodeScanner) {
      this.getMemberPoints();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransferPage');
  }

  getMemberPoints() {
    let $this = this;

    this.memberProvider.getBalance(null).then((points:any) => {
      $this.balance = parseFloat(points.Balance).toFixed(0);
    });
  }

  setMaxAmount() {
    this.amount = this.balance;
  }


  transferFunds() {
    let $this = this;
    
    if ($this.address == '' || $this.amount == '' || $this.amount == 0) {
      let toast = $this.toastCtrl.create({ 
        duration:6000, 
        message: 'There was an issue transferring, please ensure you have sufficient balance and the username you are sending to is correct'
      })
      toast.present();

      return false;
    }

    this.toCurrencyPipe.transform(this.amount,200).then((converted) => { 
      let confirm = this.alertCtrl.create({
        title: 'Transfer Confirmation',
        message: '<div text-center><h5>Transferring</h5>' + converted + ' <h5>Recipient</h5><p text-center>' + $this.address.toLowerCase() + '</p></div>',
        buttons: [
          {
            text: 'Cancel',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'Confirm',
            handler: () => {
              $this.memberProvider.transfer($this.amount, $this.address).then(() =>{
                let toast = $this.toastCtrl.create({ duration:3000, message: 'Transfer success'})
                toast.present();

                $this.viewCtrl.dismiss();
              }).catch(() =>{
                let toast = $this.toastCtrl.create({ 
                  duration:6000, 
                  message: 'There was an issue transferring, please ensure you have sufficient balance and the username you are sending to is correct'
                })
                toast.present();
              })
            }
          }
        ]
      });
      confirm.present();
    });
  }


  scanQrCode() {
    let $this = this;
    
    this.barcodeScanner.scan({ resultDisplayDuration: 0}).then(barcodeData => {
      let address = JSON.parse(barcodeData.text);
      $this.address = address.address;
    }).catch(err => {
        console.log('Error', err);
    });
  }


  dismiss() {
    this.viewCtrl.dismiss();
  }
}
