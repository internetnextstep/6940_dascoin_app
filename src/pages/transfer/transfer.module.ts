import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransferPage } from './transfer';
import { MemberProvider } from '../../providers/member/member';
import { PipesModule } from '../../pipes/pipes.module';
import { ToCurrencyPipe } from '../../pipes/to-currency/to-currency';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

@NgModule({
  declarations: [
    TransferPage,
  ],
  imports: [
    IonicPageModule.forChild(TransferPage),
    PipesModule
  ],
  providers: [
    MemberProvider,
    ToCurrencyPipe,
    BarcodeScanner
  ]
})
export class TransferPageModule {}
