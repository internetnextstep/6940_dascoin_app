import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, ViewController, ModalController } from 'ionic-angular';
import { VoucherProvider } from '../../providers/voucher/voucher';
import { CurrencyProvider } from '../../providers/currency/currency';
/**
 * Generated class for the VoucherHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-voucher-history',
  templateUrl: 'voucher-history.html'
})
export class VoucherHistoryPage {
  vouchers: any = [];
  voucherHistoryView:any = 'active';
  isLoading:any = true;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public voucherProvider: VoucherProvider,
    public currencyProvider: CurrencyProvider,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public viewCtrl : ViewController,
    public modalCtrl: ModalController
    ) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VoucherHistoryPage');
    this.getHistory();
  }

  getHistory() {
    this.isLoading = true;

    this.voucherProvider.voucherHistory().then((history) => {
      this.vouchers = history;
      this.isLoading = false;
    })
  }

  cancelVoucher(event, voucher) {
    let $this = this;

    event.stopPropagation();
    

    let confirm = this.alertCtrl.create({
      title: 'Would you like to void this voucher?',
      message: 'Doing so will refund the amount at time of generation into your DasPoint account',
      buttons: [
        {
          text: 'Close',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes, Void Voucher',
          handler: () => {
            $this.isLoading = true;
            $this.voucherProvider.cancelVoucher(voucher.voucherId, voucher.voucherCode).then((r) => {
              let toast = $this.toastCtrl.create({
                message: 'Voucher Cancelled',
                duration: 4000,
                position: 'bottom'
              });

              toast.present();

              $this.getHistory();
            });
          }
        }
      ]
    });
    confirm.present();
  }

  showVoucher(event,voucher) {
    event.stopPropagation();
    let modal = this.modalCtrl.create('VoucherImagePage', voucher);
    modal.present();

    modal.onDidDismiss(() => {
      this.getHistory();
    })
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
