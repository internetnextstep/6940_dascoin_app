import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VoucherHistoryPage } from './voucher-history';
import { PipesModule } from '../../pipes/pipes.module';
import { VoucherProvider } from '../../providers/voucher/voucher';
import { CurrencyProvider } from '../../providers/currency/currency';


@NgModule({
  declarations: [
    VoucherHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(VoucherHistoryPage),
    PipesModule
  ],
  providers: [VoucherProvider, CurrencyProvider]
})
export class VoucherHistoryPageModule {}
