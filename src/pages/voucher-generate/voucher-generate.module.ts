import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VoucherGeneratePage } from './voucher-generate';
import { PipesModule } from '../../pipes/pipes.module';
import { QRCode } from 'qrcode-generator-ts/js';
import { CurrencyProvider } from '../../providers/currency/currency';

@NgModule({
  declarations: [
    VoucherGeneratePage,
  ],
  imports: [
    IonicPageModule.forChild(VoucherGeneratePage),
    PipesModule
  ],
  providers: [QRCode,CurrencyProvider]
})
export class VoucherGeneratePageModule {}
