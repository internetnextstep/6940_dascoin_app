import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, ModalController, ViewController, ToastController, LoadingController,AlertController } from 'ionic-angular';
import { VoucherProvider } from '../../providers/voucher/voucher';
import { MemberProvider } from '../../providers/member/member';
import { QRCode, ErrorCorrectLevel, QRNumber, QRAlphaNum, QR8BitByte, QRKanji } from 'qrcode-generator-ts/js';
import { CurrencyProvider } from '../../providers/currency/currency';

@IonicPage()
@Component({
  selector: 'page-voucher-generate',
  templateUrl: 'voucher-generate.html',
})
export class VoucherGeneratePage {
  zdpBalance: any;
  zdpConversion: any;
  availableDiscounts: any;
  qrImageUri: any = null;
  currencyTy: any = 0;
  currencySymbol: any = 0;
  pageTitle:any = 'Create Voucher';  
  availableCurrencies:any;
  qrCodeGenerated:any = false;

  @ViewChild(Slides) slides: Slides;


  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public voucherProvider: VoucherProvider,
    public viewCtrl: ViewController,
    public memberProvider: MemberProvider,
    public qrcode: QRCode,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public currencyProvider: CurrencyProvider) {
    this.zdpBalance = navParams.get('zdpBalance');
    this.zdpConversion = navParams.get('zdpConversion');
    this.availableDiscounts = [5,10, 15, 20, 25 ];

    this.memberProvider.getUser().then((user:any) => {
      this.currencyTy = user.currencyTy;
      this.currencySymbol = user.currencySymbol;
    });

    this.currencyProvider.getLocalCurrencies().then((currencies) => {
      this.availableCurrencies = currencies;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VoucherGeneratePage');
  }

  createVoucher(discount) {
    let $this = this;
    
    let loader = $this.loadingCtrl.create({
      content: "Creating Voucher",
      duration: 30000
    });

    loader.present();

    $this.voucherProvider.createVoucher(discount, $this.currencyTy).then((voucher:any) => {
      $this.dismiss();
      loader.dismiss();
      voucher.amount = discount;
      voucher.currencyTy = $this.currencyTy;
      
      let modal = $this.modalCtrl.create('VoucherImagePage', voucher);
      modal.present();
      
    });
  }

  confirmVoucherGenerate(discount) {
    let $this = this;

    let confirm = this.alertCtrl.create({
      title: 'Are you sure you want to create a voucher?',
      message: 'Creating this voucher will deduct '+ $this.currencySymbol + discount +' from your balance and will be transferred to the vendor once redeemed',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Create',
          handler: () => {
            $this.createVoucher(discount);
          }
        }
      ]
    });
    confirm.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  updateBalance($event) {
    let $this = this;
    $this.memberProvider.getBalance($this.currencyTy).then((points:any) => {
      $this.zdpBalance = points.Balance;  
      $this.zdpConversion = points.BalanceInToCurrency;
    });
  }

  
}
