import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  tab1: any;
  tab2: any;
  tab3: any;
  tab4: any;
  isBookmark:any = true;

  constructor(
    public navCtrl: NavController,
    public splashScreen: SplashScreen
  ) {
    this.tab1 = 'PointsPage';
    this.tab2 = 'VendorsPage';
    this.tab3 = 'ProfilePage';
    this.tab4 = 'VendorsPage';
  }

  ionViewDidLoad() {
    console.log('home tabs loaded');
    this.splashScreen.hide();
  }

}
