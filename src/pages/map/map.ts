import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import {MapComponent } from '../../components/map/map';
import { Geolocation } from '@ionic-native/geolocation';
/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {
  @ViewChild(MapComponent) mapComponent:any;

  markers:any[] = [];
  userLocation: any = null
  isLoading:any = false;

  constructor(  
    public navCtrl: NavController, 
    public navParams: NavParams,
    public platform: Platform,
    public viewCtrl: ViewController,
    public geolocation: Geolocation) {

      this.markers = this.navParams.get('markers');
      this.userLocation = this.navParams.get('userLocation');
  }

  ionViewDidLoad() {
    let $this = this;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  findUser() {
    let $this = this;
    $this.isLoading = true;
    $this.geolocation.getCurrentPosition().then((resp) => {
      this.mapComponent.addUserLocation([resp.coords.latitude, resp.coords.longitude]).then(() => {
        $this.isLoading = false;
      });
    }).catch((error) => {
      $this.isLoading = false;
    });
    
  }

}
