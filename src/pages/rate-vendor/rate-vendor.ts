import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, ToastController, ViewController, NavParams, ModalController } from 'ionic-angular';
import { VendorProvider } from '../../providers/vendor/vendor';
/**
 * Generated class for the RateVendorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rate-vendor',
  templateUrl: 'rate-vendor.html',
})
export class RateVendorPage {
  rating:any = 0;
  vendor: any;
  feedback:any = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public vendorProvider: VendorProvider,
    public toastCtrl: ToastController) {

    this.rating = this.navParams.get('rating');
    this.vendor = this.navParams.get('vendor');
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad RateVendorPage');
  }

  // rate function
  rate(stars) {
    this.rating = stars;
  }

  // make array with range is n
  range(n) {
    return new Array(Math.round(n));
  }

  saveReview() {
    let $this = this;
    let params = {
      feedback: this.feedback,
      rating: this.rating,
      vendorId: this.vendor.vendorId
    }

    this.vendorProvider.saveReview(params).then((review) => {
      $this.feedback = '';
      $this.rating = 0;

      $this.getReviews();
      
      let toast = $this.toastCtrl.create({ message: 'Thank-you for your feedback', duration: 2500 });
      toast.present();

    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  getReviews() {
    this.vendorProvider.getReviews({ vendorId: this.vendor.vendorId}).then((reviews) =>{
      this.vendor.reviews = reviews;
    });
  }
}
