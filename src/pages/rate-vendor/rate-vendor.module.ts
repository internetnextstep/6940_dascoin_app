import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RateVendorPage } from './rate-vendor';
import { VendorProvider } from '../../providers/vendor/vendor';

@NgModule({
  declarations: [
    RateVendorPage,
  ],
  imports: [
    IonicPageModule.forChild(RateVendorPage),
  ],
  providers: [ VendorProvider]
})
export class RateVendorPageModule {}
