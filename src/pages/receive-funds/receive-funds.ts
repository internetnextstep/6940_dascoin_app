import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController, ViewController } from 'ionic-angular';
import { QRCode, ErrorCorrectLevel, QRNumber, QRAlphaNum, QR8BitByte, QRKanji } from 'qrcode-generator-ts/js';
import { Clipboard } from '@ionic-native/clipboard';
import { MemberProvider } from '../../providers/member/member';

/**
 * Generated class for the ReceiveFundsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-receive-funds',
  templateUrl: 'receive-funds.html',
  providers: [QRCode]
})
export class ReceiveFundsPage {
  qrImageUri:any = '';
  address: any = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public qrcode: QRCode,
    public toastCtrl: ToastController,
    public clipboard: Clipboard,
    public viewCtrl: ViewController,
    public memberProvider: MemberProvider
    ) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReceiveFundsPage');

    this.getAddress();
  }

  getAddress() {
    this.memberProvider.getAddress().then((address) => {
      this.address = address;
      this.generateQR({ address: this.address });
    });
  }

  generateQR(data) {
    let typeNumber =10;
    let errorCorrectionLevel = 'L';
    
    this.qrcode.setErrorCorrectLevel(ErrorCorrectLevel.M);
    this.qrcode.setTypeNumber(typeNumber);
    this.qrcode.clearData();
    this.qrcode.addData(JSON.stringify(data));
    this.qrcode.make();
    this.qrImageUri = this.qrcode.toDataURL(4);
  };

  copyAddress(text) {
    this.clipboard.copy(text);

    let toast = this.toastCtrl.create({ message: 'Address copied to clipboard', duration: 3000});
    toast.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
