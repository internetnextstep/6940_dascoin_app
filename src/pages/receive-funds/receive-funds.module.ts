import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceiveFundsPage } from './receive-funds';
import { Clipboard } from '@ionic-native/clipboard';
import { MemberProvider } from '../../providers/member/member';

@NgModule({
  declarations: [
    ReceiveFundsPage,
  ],
  imports: [
    IonicPageModule.forChild(ReceiveFundsPage),
  ],
  providers: [Clipboard,MemberProvider]
})
export class ReceiveFundsPageModule {}
