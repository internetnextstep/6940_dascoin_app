import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PointsPage } from './points';
import { MemberProvider } from '../../providers/member/member';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Camera } from '@ionic-native/camera';
import { ReceiptProvider } from '../../providers/receipt/receipt';
@NgModule({
  declarations: [
    PointsPage,
  ],
  imports: [
    IonicPageModule.forChild(PointsPage),
  ],
  providers: [MemberProvider,BarcodeScanner,Camera, ReceiptProvider]
})
export class PointsPageModule {}
