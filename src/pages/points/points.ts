import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, ToastController, PopoverController  } from 'ionic-angular';
import { MemberProvider } from '../../providers/member/member';
import { VoucherProvider } from '../../providers/voucher/voucher';
import { VendorProvider } from '../../providers/vendor/vendor';
import { ReceiptProvider } from '../../providers/receipt/receipt';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Camera, CameraOptions } from '@ionic-native/camera';
/**
 * Generated class for the PointsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({ name: 'PointsPage' })
@Component({
  selector: 'page-points',
  templateUrl: 'points.html',
})


export class PointsPage {
  zdpBalance:any = 0;
  zdpConversion:any = 0;
  isLoadingBalance:any = true;
  currencySymbol:any = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public memberProvider: MemberProvider,
    public modalCtrl: ModalController,
    public barcodeScanner: BarcodeScanner,
    public voucherProvider: VoucherProvider,
    public alertCtrl: AlertController,
    public camera: Camera,
    public receiptProvider: ReceiptProvider,
    public toastCtrl: ToastController,
    public popoverCtrl: PopoverController,
    public vendorProvider: VendorProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PointsPage');

    this.getMemberPoints();

    this.memberProvider.getUser().then((user:any) => {
      this.currencySymbol = user.currencySymbol;
    });
  }

  getMemberPoints() {
    let $this = this;
    $this.isLoadingBalance = true;

    this.memberProvider.getBalance(null).then((points:any) => {
      $this.zdpBalance = parseInt(points.Balance).toFixed(0);  
      $this.zdpConversion = parseFloat(points.BalanceInToCurrency).toFixed(2);  
      $this.isLoadingBalance = false;
    });
  }

  onSlideChanged() {
    this.getMemberPoints();
  }

  showVoucherPage() {
    let $this = this;
    let modal = this.modalCtrl.create('VoucherGeneratePage', {zdpBalance: this.zdpBalance, zdpConversion: this.zdpConversion });
    modal.present();

    modal.onDidDismiss(()=>{
      $this.getMemberPoints()
    });
  }

  showTransferPage() {
    let $this = this;
    let modal = this.modalCtrl.create('TransferPage');
    modal.present();

    modal.onDidDismiss(()=>{
      $this.getMemberPoints()
    });
  }

  showTransactionsPage() {
    let $this = this;
    let modal = this.modalCtrl.create('TransactionsPage');
    modal.present();

    modal.onDidDismiss(()=>{
      $this.getMemberPoints()
    });
  }

  showVoucherHistoryPage() {
    let $this = this;
    let modal = this.modalCtrl.create('VoucherHistoryPage');
    modal.present();

    modal.onDidDismiss(()=>{
    });
  }

  showReceivePage() {
    let $this = this;
    let modal = this.modalCtrl.create('ReceiveFundsPage');
    modal.present();

    modal.onDidDismiss(()=>{
      $this.getMemberPoints()
    });
  }

  scanReceiptConfirm() {
    let $this = this;

    let confirm = this.alertCtrl.create({
        title: 'Scan receipts and receive points?',
        message: 'Scanned receipts get sent to our team for approval. Once approved, you will be rewarded points.',
        buttons: [
          {
            text: 'Cancel',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'Scan',
            handler: () => {
              $this.scanReceipt();
            }
          }
        ]
      });
      confirm.present();

  }

  scanReceipt()  {
    let $this = this;

    $this.vendorProvider.getList({}).then((list) => {
      let popover = $this.popoverCtrl.create('VendorListPage', list);
      popover.present();

      popover.onDidDismiss((vendor) => {
        const options: CameraOptions = {
          quality: 50,
          destinationType: this.camera.DestinationType.FILE_URI,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE
        }

        this.camera.getPicture(options).then((imageUrl) => {
          // imageData is either a base64 encoded string or a file URI
          // If it's base64:
          $this.receiptProvider.upload(vendor.vendorId, imageUrl).then((r) => {
            $this.toastCtrl.create({ 
              message: 'Receipt submitted for approval, you will be notified once approved',
              duration: 3000
            });
          });
        }, (err) => {
          // Handle error
        });
      });
    });    
  }

  scanQrCode() {
    let $this = this;
    
    this.barcodeScanner.scan({ resultDisplayDuration: 0}).then(barcodeData => {
      let voucher = JSON.parse(barcodeData.text);

      let confirm = this.alertCtrl.create({
        title: 'Would you like to redeem this voucher?',
        message: 'You are about to redeem this voucher of ' + voucher.amount + '. Would you like to continue',
        buttons: [
          {
            text: 'Cancel',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'Redeem',
            handler: () => {
              $this.voucherProvider.redeemVoucher(voucher.voucherId, voucher.voucherCode).then(() => {
              });
            }
          }
        ]
      });
      confirm.present();
     

    }).catch(err => {
        console.log('Error', err);
    });
  }
}
