import { Component,Renderer2 } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Ionic2RatingModule } from "ionic2-rating";

/**
 * Generated class for the VendorReviewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vendor-reviews',
  templateUrl: 'vendor-reviews.html',
})
export class VendorReviewsPage {
  vendor:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public renderer: Renderer2) {
      
    this.vendor = this.navParams.get('vendor');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VendorReviewsPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  expandReview(event) {
   // this.renderer.addClass(event.target, 'expanded');
  }
}
