import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VendorReviewsPage } from './vendor-reviews';
import { Ionic2RatingModule } from "ionic2-rating";
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    VendorReviewsPage,
  ],
  imports: [
    IonicPageModule.forChild(VendorReviewsPage),
    Ionic2RatingModule,
    PipesModule
  ],
})
export class VendorReviewsPageModule {}
