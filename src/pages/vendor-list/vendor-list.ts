import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { VendorProvider } from '../../providers/vendor/vendor';

/**
 * Generated class for the VendorListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vendor-list',
  templateUrl: 'vendor-list.html',
})
export class VendorListPage {
  vendor_list:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public vendorProvider: VendorProvider,
    public viewCtrl: ViewController) {

    this.vendor_list = this.navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VendorListPage');
  }

  select(vendor) {
    this.viewCtrl.dismiss(vendor);
  }

}
