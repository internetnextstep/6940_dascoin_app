import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VendorListPage } from './vendor-list';
import { VendorProvider } from '../../providers/vendor/vendor';
import { PipesModule } from '../../pipes/pipes.module';
@NgModule({
  declarations: [
    VendorListPage,
  ],
  imports: [
    IonicPageModule.forChild(VendorListPage),
    PipesModule
  ],
  providers: [VendorProvider]
})
export class VendorListPageModule {}
