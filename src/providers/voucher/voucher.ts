import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppCommon } from '../app';
import { Storage } from '@ionic/storage';

/*
  Generated class for the VoucherProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class VoucherProvider {

  constructor(
    public http: HttpClient,
    public appCommon: AppCommon,
    public storage: Storage
  ) {
    console.log('Hello VoucherProvider Provider');
  }

  createVoucher(amount, currencyTy) {
    let $this = this;

    // don't have the data yet
    return new Promise(function(resolve,reject) {
       $this.storage.get('user').then((user) => {
        $this.http.post($this.appCommon.API_ENDPOINT + '/custom/voucher', { 
          userId: user.userId, 
          guid: user.guid, 
          amount: amount , 
          currencyTy: 0
        })
        .subscribe((data:any) => {

          if (data.status == 'ok') {
            resolve(data.data);       
          } else {
            reject();
          }

        }, error => {
            console.log('iserror:' + JSON.stringify(error)); 
        });
      });
    });
  }

  isVoucherRedeemed(voucherId, voucherCode) {
    let $this = this;

    // don't have the data yet
    return new Promise(function(resolve,reject) {
       $this.storage.get('user').then((user) => {
        $this.http.get($this.appCommon.API_ENDPOINT + '/custom/voucher/redeemed', { 
          params: {
            userId: user.userId, 
            guid: user.guid, 
            voucherId: voucherId , 
            voucherCode: voucherCode
          }
        })
        .subscribe((data:any) => {

          if (data.status == 'ok') {
            resolve(data.data);       
          } else {
            reject();
          }

        }, error => {
            console.log('iserror:' + JSON.stringify(error)); 
        });
      });
    });
  }

  redeemVoucher(voucherId, voucherCode) {
    let $this = this;

    // don't have the data yet
    return new Promise(function(resolve,reject) {
       $this.storage.get('user').then((user) => {
        $this.http.post($this.appCommon.API_ENDPOINT + '/custom/voucher/redeem', { 
            userId: user.userId, 
            guid: user.guid, 
            voucherId: voucherId, 
            voucherCode: voucherCode      
        })
        .subscribe((data:any) => {

          if (data.status == 'ok') {
            resolve(data.data);       
          } else {
            reject();
          }

        }, error => {
            console.log('iserror:' + JSON.stringify(error)); 
        });
      });
    });
  }

  cancelVoucher(voucherId, voucherCode) {
    let $this = this;

    // don't have the data yet
    return new Promise(function(resolve,reject) {
       $this.storage.get('user').then((user) => {
        $this.http.post($this.appCommon.API_ENDPOINT + '/custom/voucher/cancel', { 
            userId: user.userId, 
            guid: user.guid, 
            voucherId: voucherId, 
            voucherCode: voucherCode      
        })
        .subscribe((data:any) => {

          if (data.status == 'ok') {
            resolve(data.data);       
          } else {
            reject();
          }

        }, error => {
            console.log('iserror:' + JSON.stringify(error)); 
        });
      });
    });
  }

  generateQR(voucherCode) {
    let $this = this;

    // don't have the data yet
    return new Promise(function(resolve,reject) {
       $this.storage.get('user').then((user) => {
        $this.http.get($this.appCommon.API_ENDPOINT + '/custom/voucher/generate', { params: {userId: user.userId, guid: user.guid, voucherCode: voucherCode }})
        .subscribe((data:any) => {

          if (data.status == 'ok') {
            resolve(data.data);       
          } else {
            reject();
          }

        }, error => {
            console.log('iserror:' + JSON.stringify(error)); 
        });
      });
    });
  }


  voucherHistory() {
    let $this = this;

    // don't have the data yet
    return new Promise(function(resolve,reject) {
       $this.storage.get('user').then((user) => {
        $this.http.get($this.appCommon.API_ENDPOINT + '/custom/voucher/history', { params: {userId: user.userId, guid: user.guid }})
        .subscribe((data:any) => {

          if (data.status == 'ok') {
            resolve(data.data);       
          } else {
            reject();
          }

        }, error => {
            console.log('iserror:' + JSON.stringify(error)); 
        });
      });
    });
  }
}
