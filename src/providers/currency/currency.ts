import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppCommon } from '../app';
import { Storage } from '@ionic/storage';

/*
  Generated class for the CurrencyProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CurrencyProvider {

  constructor(
    public http: HttpClient,
    public appCommon: AppCommon,
    public storage: Storage) {
    console.log('Hello CurrencyProvider Provider');
  }

  getCurrencies() {
    let $this = this;

    return new Promise(function(resolve,reject) {
       $this.http.get($this.appCommon.API_ENDPOINT + '/custom/currencies')
        .subscribe((data:any) => {
          if (data.status == 'ok') {  
              $this.storage.set('currencies', data.data);           
              resolve(data.data);            
          } else {
            reject();
          }

        }, error => {
            console.log('iserror:' + JSON.stringify(error)); 
        });
    });
  }

  getLocalCurrencies() {
    let $this = this;
    
    return new Promise(function(resolve,reject) {
      $this.storage.get('currencies').then((currencies) => {
        resolve(currencies);
      });  
    });
  }

}
