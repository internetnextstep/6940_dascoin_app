import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppCommon } from '../app';
import { Storage } from '@ionic/storage';

/*
  Generated class for the MemberProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MemberProvider {

  constructor(
    public http: HttpClient,
    public appCommon: AppCommon,
    public storage: Storage
  ) {
    console.log('Hello MemberProvider Provider');
  }

  login(username, password) {
    let $this = this;

    // don't have the data yet
    return new Promise(function(resolve,reject) {
        $this.http.post($this.appCommon.API_ENDPOINT + '/v2/login/members', { 
          handle: username, 
          password: password 
        })
        .subscribe((data:any) => {

          if (data.status == 'ok') {
            $this.storage.set('user', data.data).then(() => {
              resolve(data.data);       
            });
          } else {
            reject();
          }

        }, error => {
            console.log('iserror:' + JSON.stringify(error)); 
        });
    });
  }

  updateCustomer(firstName, lastName, email, phone) {
    let $this = this;

    // don't have the data yet
    return new Promise(function(resolve,reject) {
        $this.storage.get('user').then((user) => {
          user.firstName = firstName;
          user.lastName = lastName;
          user.email = email;
          user.phone = phone;

          $this.http.post($this.appCommon.API_ENDPOINT + '/custom/member/update', { 
            firstName: firstName, 
            lastName: lastName,
            email: email,
            phone: phone,
            userId: user.userId
          })
          .subscribe((data:any) => {

            if (data.status == 'ok') {
              $this.storage.set('user', user).then(() => {
                resolve(user);       
              });
            } else {
              reject();
            }

          }, error => {
              console.log('iserror:' + JSON.stringify(error)); 
          });
        });
    });
  }

  getBalance(currencyTy) {
    let $this = this;

    return new Promise(function(resolve,reject) {
      $this.storage.get('user').then((user) => {
          currencyTy = currencyTy === null ? user.currencyTy : currencyTy;

          // don't have the data yet        
          $this.http.get($this.appCommon.API_ENDPOINT + '/custom/member/balance', { params: { userId: user.userId, currencyTy:currencyTy}})
          .subscribe((data:any) => {

            if (data.status == 'ok') {             
                resolve(data.data);       
             
            } else {
              reject();
            }

          }, error => {
              console.log('iserror:' + JSON.stringify(error)); 
          });
      });
    });
  }

   getTransactions(currencyTy) {
    let $this = this;

    return new Promise(function(resolve,reject) {
      $this.storage.get('user').then((user) => {
          currencyTy = currencyTy === null ? user.currencyTy : currencyTy;

          // don't have the data yet        
          $this.http.get($this.appCommon.API_ENDPOINT + '/custom/member/transactions', { 
            params: { 
              userId: user.userId, 
              currencyTy:currencyTy,
              guid: user.guid
            }
          })
          .subscribe((data:any) => {

            if (data.status == 'ok') {             
                resolve(data.data);       
             
            } else {
              reject();
            }

          }, error => {
              console.log('iserror:' + JSON.stringify(error)); 
          });
      });
    });
  }


  getUser() {
    let $this = this;

    return new Promise(function(resolve,reject) {
      $this.storage.get('user').then((user) => {
        resolve(user);
      });
    });
  }

  getAddress() {
    let $this = this;

    return new Promise(function(resolve,reject) {
      $this.storage.get('user').then((user) => {
         $this.http.get($this.appCommon.API_ENDPOINT + '/custom/member/address', { params: { 
            guid: user.guid,
            userId: user.userId
          }})
          .subscribe((data:any) => {
            resolve(data.data);
          });
      });
    });
  }

  transfer(amount, recipient) {
    let $this = this;

    return new Promise(function(resolve,reject) {
      // Hardcoded to 200 only (ZDP)
      $this.getUser().then((user:any) => {

        $this.getBalance(200).then((r:any) => {
          if (parseFloat(r.Balance) >= amount) {
            $this.http.post($this.appCommon.API_ENDPOINT + '/custom/member/transfer', { 
              userId: user.userId, 
              amount:amount, 
              address: recipient,
              guid: user.guid
            })
            .subscribe((data:any) => {

              if (data.status == 'ok') {    
                if (data.data.Result.toLowerCase() == 'failed') {
                  reject();
                  return false;
                }         

                resolve(data.data);                    
              } else {
                reject();
              }

            }, error => {
                console.log('iserror:' + JSON.stringify(error)); 
            });
          } else {
            reject();
          }
        });

      });
    });
  }

}
