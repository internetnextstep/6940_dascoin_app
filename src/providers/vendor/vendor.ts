import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppCommon } from '../app';
import 'rxjs/add/operator/timeout'; 
import { Storage } from '@ionic/storage';
import * as _ from 'underscore';

/*
  Generated class for the VendorProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class VendorProvider {

  constructor(
    public http: HttpClient,
    public appCommon: AppCommon,
    public storage: Storage
  ) {
    console.log('Hello VendorProvider Provider');
  }

  getList(opts) {
    let $this = this;
    // don't have the data yet
    return new Promise(function(resolve,reject) {
        $this.http.get($this.appCommon.API_ENDPOINT + '/custom/vendors', { params: opts})
        //.timeout(15)
        .subscribe((data:any) => {

          if (data.status == 'ok') {
            resolve(data.data);       
          } else {
            reject();
          }

        }, error => {
            
            console.log('iserror:' + JSON.stringify(error)); 

            reject();
        });
    });
  }

  saveReview(opts) {
    let $this = this;

    
    // don't have the data yet
    return new Promise(function(resolve,reject) {
       $this.storage.get('user').then((user) => {
          opts.userId = user.userId;
          opts.guid = user.guid;

          let review = {
            name: user.firstName,
            comment: opts.feedback,
            avatarUrl: user.avatarUrl === undefined ? '' : user.avatarUrl,
            rating: opts.rating
          };

          $this.http.post($this.appCommon.API_ENDPOINT + '/custom/vendor/review', opts)
          //.timeout(15)
          .subscribe((data:any) => {

            if (data.status == 'ok') {
              resolve(review);       
            } else {
              reject();
            }

          }, error => {
              
              console.log('iserror:' + JSON.stringify(error)); 

              reject();
          });
        });
    });
  }

  getReviews(opts) {
    let $this = this;

    
    // don't have the data yet
    return new Promise(function(resolve,reject) {
       $this.storage.get('user').then((user) => {
          opts.userId = user.userId;
          opts.guid = user.guid;

          $this.http.get($this.appCommon.API_ENDPOINT + '/custom/vendor/reviews', { params:  opts})
          //.timeout(15)
          .subscribe((data:any) => {

            if (data.status == 'ok') {
              resolve(data.data);       
            } else {
              reject();
            }

          }, error => {
              
              console.log('iserror:' + JSON.stringify(error)); 

              reject();
          });
        });
    });
  }

  saveBookmark(vendorId) {
    let $this = this;
    return new Promise(function(resolve,reject) {
      $this.isVendorBookmarked(vendorId).then((isBookmarked) => {
        if (isBookmarked === false) {
          $this.storage.get('bookmarks').then((bookmarks) =>{
            bookmarks = bookmarks === null ? [] : bookmarks;

            bookmarks.push(vendorId);

            $this.storage.set('bookmarks', _.uniq(bookmarks)).then(() => {
              resolve();        
            })
          });
        } else {
          $this.storage.set('bookmarks', [vendorId]).then(() => {
            resolve();        
          })
        }
      });
    });
  }

  putBookmark(vendorId) {
    let $this = this;
    
    return new Promise(function(resolve,reject) {
      $this.storage.get('user').then((user) => {
        $this.http.post($this.appCommon.API_ENDPOINT + '/custom/member/vendor/bookmark', { id:user.userId, guid: user.guid, vendorId: vendorId})
        .subscribe((data:any) => {

          if (data.status == 'ok') {
            $this.saveBookmark(vendorId).then(()=>{
                resolve(); 
            });       
          } else {
            reject();
          }

        }, error => {
            
            console.log('iserror:' + JSON.stringify(error)); 

            reject();
        });
      });

    });
  }

  getBookmarks() {
    let $this = this;

    return new Promise(function(resolve,reject) {
      $this.storage.get('user').then((user) => {
        $this.http.get($this.appCommon.API_ENDPOINT + '/custom/member/vendor/bookmarks', { 
          params: {
            id: user.userId,
            guid:user.guid,
          }
        }).subscribe((data:any) => {

          if (data.status == 'ok') {
            $this.storage.set('bookmarks', data.data).then(() => {
                resolve(data.data);       
              })
          } else {
            reject();
          }

        }, error => {
            
            console.log('iserror:' + JSON.stringify(error)); 

            reject();
        });
      });
    });
  }

  deleteBookmark(vendorId) {
    let $this = this;

    return new Promise(function(resolve,reject) {
      $this.storage.get('user').then((user) => {
        $this.http.post($this.appCommon.API_ENDPOINT + '/custom/member/vendor/bookmark/delete', { id:user.userId, guid: user.guid, vendorId: vendorId})
        .subscribe((data:any) => {

          if (data.status == 'ok') {
             $this.storage.get('bookmarks').then((bookmarks) =>{
              bookmarks = bookmarks.filter(item => item.id !== vendorId);

              $this.storage.set('bookmarks', bookmarks).then(() => {;
                resolve( bookmarks );        
              });
            });
          } else {
            reject();
          }

        }, error => {
            
            console.log('iserror:' + JSON.stringify(error)); 

            reject();
        });
      });

    });
  }

  isVendorBookmarked(vendorId) {
    let $this = this;
    let found = false;

    return new Promise(function(resolve,reject) {
      $this.getBookmarks().then((bookmarks:any) => {
        if (bookmarks !== null) {
          for (var i = 0, len = bookmarks.length; i < len; i++) {
            if (parseInt(bookmarks[i].vendorId) === parseInt(vendorId)) {
                found = true;
                resolve(true);
                break; 
            }
          }
          
          if (found === false) {
            resolve(false);
          }
        } else {
          console.log('no bookmarks');
          resolve(false);
        }
      });
    });
  }

  getVendorTypes() {
    let $this = this;

    return new Promise(function(resolve,reject) {
      $this.storage.get('user').then((user) => {
        $this.http.get($this.appCommon.API_ENDPOINT + '/custom/vendor/types', { 
          params: {
            id: user.userId,
            guid:user.guid,
          }
        }).subscribe((data:any) => {

          if (data.status == 'ok') {
            resolve(data.data);       
          } else {
            reject();
          }
        }, error => {
            console.log('iserror:' + JSON.stringify(error)); 

            reject();
        });
      });
    });
  }
}
