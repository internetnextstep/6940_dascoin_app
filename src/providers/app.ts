import {Injectable} from '@angular/core';
import { Platform } from 'ionic-angular';
//import 'rxjs/add/operator/map';

@Injectable()
export class AppCommon {

API_ENDPOINT :string;
APP_VERSION : string;
GA_TRACKER_ID: string;
ENV : string;
SYSTEM_URL: string;

  constructor(
      public platform: Platform
  ) {
    this.SYSTEM_URL = 'https://smartbusinesstools.com';
    this.API_ENDPOINT = this.SYSTEM_URL + '/api/index.php';
    this.ENV = 'app';
    this.APP_VERSION = '0.0.6';
  }
}