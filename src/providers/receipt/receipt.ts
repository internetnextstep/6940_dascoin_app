import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AppCommon } from '../app';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

/*
  Generated class for the ReceiptProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ReceiptProvider {

  constructor(
    public http: HttpClient,
    public storage: Storage,
    public appCommon: AppCommon,
    public fileTransfer: FileTransfer,
    public file: File) {
    console.log('Hello ReceiptProvider Provider');
  }

  put(vendorId,imageUrl) {
    let $this = this;
    
    // don't have the data yet
    return new Promise(function(resolve,reject) {
       $this.storage.get('user').then((user) => {
        $this.http.post($this.appCommon.API_ENDPOINT + '/custom/receipt', { 
            userId: user.userId, 
            guid: user.guid, 
            vendorId: vendorId 
        })
        .subscribe((data:any) => {

          if (data.status == 'ok') {
            resolve(data.data);       
          } else {
            reject();
          }

        }, error => {
            console.log('iserror:' + JSON.stringify(error)); 
        });
      });
    });
  }

  upload(vendorId, imageUrl) {
    let $this = this;
    
    return new Promise(function(resolve,reject) {
      $this.storage.get('user').then((user) => {
        const fileTransfer: FileTransferObject = $this.fileTransfer.create();
        var targetPath = $this.file.dataDirectory + imageUrl;
        var pieces = imageUrl.split("/");
        var fileName = pieces[pieces.length -1];

        let options: FileUploadOptions = {
          fileKey: 'file',
          fileName: fileName,
          chunkedMode: false,
          params: { 
            vendorId: vendorId, 
            purchaserId: user.userId
          }
        }

        fileTransfer.upload(imageUrl, $this.appCommon.API_ENDPOINT + '/custom/receipt/upload', options)
          .then((data:any) => {

            /*
            this.put(vendorId, imageUrl).then((r) => {
              
            });
            */

            resolve();
          }, (err) => {
            console.log(err);
            reject();
        });
      });    
    });    
  }

}
