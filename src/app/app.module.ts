import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'; 
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

import { AppCommon } from '../providers/app';
import { MemberProvider } from '../providers/member/member';
import { VendorProvider } from '../providers/vendor/vendor';
import { VoucherProvider } from '../providers/voucher/voucher';
import { CurrencyProvider } from '../providers/currency/currency';
import { ReceiptProvider } from '../providers/receipt/receipt';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: true,
      mode: 'md'
    }),
    IonicStorageModule.forRoot(),
    HttpClientModule
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MemberProvider,
    AppCommon,
    VendorProvider,
    VoucherProvider,
    CurrencyProvider,
    ReceiptProvider,
    FileTransfer,
    File
  ]
})
export class AppModule {}
