import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { Storage } from '@ionic/storage';
import { CurrencyProvider } from '../providers/currency/currency';

@Component({
  templateUrl: 'app.html',
  providers: [CurrencyProvider]
})
export class MyApp {
  rootPage:any = 'LoginPage';

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    public storage: Storage,
    public currencyProvider: CurrencyProvider) {
    
    let $this = this;

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      
      statusBar.styleDefault();

      $this.storage.get('user').then((user) => {
        if (user !== null) {
          $this.rootPage = 'HomePage';
        }

        $this.currencyProvider.getCurrencies();
      });

    });
  }
  
}
